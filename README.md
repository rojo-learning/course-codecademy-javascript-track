# Codecademy's JavaScript Track

This repository contains code generated from the projects of the JavaScript track in
[Codecademy](http://www.codecademy.com/tracks/javascript). These are little practices but, most of the time, these are improved versions as result of the development of functionality suggested on the lessons.

## List of Projects
 - **Choose Your Own Adventure!** We're going to show you how to program a 'choose your own adventure' game. It'll have a basic story line, have the user make some choices, and have a happy ending.
 - **Build Rock, Paper, Scissors** Rock paper scissors is a classic 2 player game. Each player chooses either rock, paper or scissors.
 - **Search Text for Your Name** In this project, you'll be writing a short program that checks a block of text for your name.
 - **Dragon Slayer** Now that you know how to use `while` loops, we'll combine them with some other control flow statements (like `if`/`else`) to create a dragon slaying mini game.
 - **Choose Your Own Adventure! 2** Now that you know more about JavaScript, you'll be able to create a much richer "choose your own adventure" game.
 - **Contact List** In this project, we'll combine our knowledge of objects and arrays to create a simple contact list. Then, using functions, we'll be able to log the entries in our contact list to the console, as well as search for a particular entry.
 - **Building an Address Book** This project makes use of objects in the context of an address book.
 - **Building a Cash Register** The cash register has failed and the boss is not happy, help save the day and create your own cash register.

---
**Note**: The description of the problems is material from Codecademy and is included in this repository under _Fair Use_. Unless otherwise stated, all the code included in this repository is released to the public domain under the [Creative Commons CC0 1.0 Universal License][10].

  [10]: https://creativecommons.org/publicdomain/zero/1.0/
