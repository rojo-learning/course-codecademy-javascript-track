
var answer = prompt("Would you rather be programming in Ruby?").toLowerCase();

switch( answer ) {
  case "yes":
    console.log("Me too, but learning JavaScript will be useful.");
    break;
  case "no":
    var ruby  = confirm("Have you tried Ruby?");
    var learn = confirm("Would you like to learn something new?");
    if ( ruby && learn ) {
      console.log("Ruby is the way! Trust me.");
    } else {
      console.log("Mmmm... I'm sure you have your reasons.");
    };
    break;
  case "maybe":
    var dynamic = confirm("Do you like dynamic laguages?");
    var clear   = confirm("Do you like programs with clear syntax?");
    if (dynamic || clear) {
      console.log("Then you should try Ruby. It's wonderful!");
    } else {
      console.log("Dude, you're so old style.");
    };
    break;
  default:
    console.log("I didn't get your answer...");
    break;
};
