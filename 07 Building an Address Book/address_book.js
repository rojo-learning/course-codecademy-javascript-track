var bob = {
    firstName: "Bob",
    lastName: "Jones",

    phoneNumber: "(650) 777 - 7777",
    email: "bob.jones@example.com"
};

var mary = {
    firstName: "Mary",
    lastName: "Johnson",

    phoneNumber: "(650) 888 - 8888",
    email: "mary.johnson@example.com"
};

var contacts = new Array(bob, mary);

function add (firstName, lastName, email, telephone) {
  contacts[contacts.length] = new Object(
    firstName, lastName, email, telephone
  );
};

function printPerson (person) {
    console.log(person.firstName + " " + person.lastName);
};

function list(){
  var length = contacts.length;
  var i;
  for(i=0;i<length;i++){
    printPerson(contacts[i]);
  };
};

/*Create a search function
then call it passing "Jones"*/
function search (lastName) {
  var i = contacts.length; while (i--) {
    if( contacts[i].lastName === lastName ) {
      printPerson(contacts[i]);
    };
  };
};

//search("Jones");

var firstName = prompt("First Name: ");
var lastName  = prompt("Last Name: ");
var email     = prompt("Email: ");
var telephone = prompt("Telephone: ");
add (firstName, lastName, email, telephone);

list();
