var slay = true;
var hit  = Math.floor(Math.random() * 2);
var roundDamage = Math.floor(Math.random() * 5 + 1);
var totalDamage = 0;

while( slay ){
  if( hit ){
    console.log("Yes! You hit the dragon!");
    console.log("The dragon received " + roundDamage + " damage.");
    totalDamage += roundDamage;

    if( totalDamage >= 4 ){
      console.log("You've slayed the dragon!");
      slay = false;
    } else {
      hit = Math.floor(Math.random() * 2);
    }
  } else {
    console.log("Too bad! The dragon got you... you're toast.");
    slay = false;
  };
};
