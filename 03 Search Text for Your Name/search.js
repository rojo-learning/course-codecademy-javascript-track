var text   = "Estoy listo para aprender la lección del día. Me he vestido con \
              una prenda roja, ya que Rojo es mi color.";
var myName = "Rojo";
var hits   = [];

for ( var i = 0; i < text.length; i++ ) {
  if ( text[i] == myName[0] ) {
    for ( var j = i; j < i + myName.length; j++ ) {
      hits.push(text[j]);
    };
  };
};

if ( hits.length !== 0 ) {
  console.log(hits);
} else {
  console.log("Your name wasn't found!");
};
