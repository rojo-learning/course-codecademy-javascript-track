var friends = {
  steve: {
    firstName: "Steve",
    lastName: "Jobs",
    number: "(555) 555-5555",
    address: ['Some street','Some district','Some state']
  },

  bill: {
    firstName: "Bill",
    lastName: "Gates",
    number: "(555) 555-5556",
    address: ['Some other street','Some other district','Some other state']
  }
};

function list(object) {
  for( var contact in object ) {
    console.log(contact);
  };
};

function search(name) {
  for ( var contact in friends ) {
    if ( friends[contact].firstName === name ) {
      console.log(friends[contact]);
      return friends[contact];
    };
  };
};

list(friends);
search("Steve");
